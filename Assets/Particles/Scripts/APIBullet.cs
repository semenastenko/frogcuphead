using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.ShadersAPI.Bullet
{
    [AddComponentMenu("Kudos/ShadersAPI/Bullet")]
    [ExecuteAlways]
    public class APIBullet : MonoBehaviour
    {
        [SerializeField] private float sizeBullet = 1f;
        [SerializeField] private Color color;

        private TrailRenderer trail;
        private ParticleSystem.MainModule beam;
        private ParticleSystem.MainModule bullet;

        private void OnValidate()
        {
            trail = transform.GetChild(0).gameObject.GetComponent<TrailRenderer>();
            beam = transform.GetChild(1).gameObject.GetComponent<ParticleSystem>().main;
            bullet = transform.GetChild(2).gameObject.GetComponent<ParticleSystem>().main;
            bullet.startSize3D = true;
            ChangeSizeBullet(sizeBullet);
            ChangeColorBullet(color);
        }
        private void Awake()
        {
            trail = transform.GetChild(0).gameObject.GetComponent<TrailRenderer>();
            beam = transform.GetChild(1).gameObject.GetComponent<ParticleSystem>().main;
            bullet = transform.GetChild(2).gameObject.GetComponent<ParticleSystem>().main;
            bullet.startSize3D = true;
            ChangeSizeBullet(sizeBullet);
            ChangeColorBullet(color);
        }

        public void ChangeTimeTrailBullet(float timeTrail, Vector3 pos)
        {
            trail.time = timeTrail;
            Vector3[] vectors = new Vector3[trail.positionCount];
            for (int i = 0; i < vectors.Length; i++)
            {
                vectors[i] = pos;
            }
            trail.SetPositions(vectors);
        }

        public void ChangeSizeBullet(float sizeBullet)
        {
            beam.startSize = sizeBullet;
            bullet.startSizeX = new ParticleSystem.MinMaxCurve(sizeBullet * 11, sizeBullet * 10);
            bullet.startSizeY = new ParticleSystem.MinMaxCurve(sizeBullet * 11, sizeBullet * 10);
            bullet.startSizeZ = sizeBullet * 10;
        }

        public void ChangeColorBullet(Color color)
        {
            bullet.startColor = color;
            beam.startColor = color;
            trail.startColor = new Color(color.r, color.g, color.b, 0.19f);
            trail.endColor = new Color(color.r, color.g, color.b, 0);
        }
    }
}
