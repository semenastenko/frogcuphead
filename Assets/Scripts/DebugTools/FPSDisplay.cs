﻿using UnityEngine;
using UnityEngine.UI;

namespace HPEW.DebugTools
{

	public class FPSDisplay : MonoBehaviour
	{
		public Text AverageFPStext;
		public Text HighestFPStext;
		public Text LowestFPStext;

		FPSCounter fpsCounter;

		void Awake()
		{
			fpsCounter = new FPSCounter();
		}

		private void Update()
		{
			AverageFPStext.text = fpsCounter.AverageFPS.ToString();
			HighestFPStext.text = fpsCounter.HighestFPS.ToString();
			LowestFPStext.text = fpsCounter.LowestFPS.ToString();
		}
	}
}