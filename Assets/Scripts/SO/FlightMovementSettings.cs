using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.SO
{
    [CreateAssetMenu(fileName = "FlightSettings", menuName = "SO/FlightSettings")]
    public class FlightMovementSettings : ScriptableObject
    {
        [Header("Plane Input Settings"), SerializeField]
        private float sensitivity = 1;
        [SerializeField]
        private float damping = -1;
        [SerializeField, Range(0, 1)]
        private float inertia = 0;

        public float Sensitivity
        {
            get => sensitivity;
            set => sensitivity = value;
        }
        public float Damping
        {
            get => damping;
            set => damping = value;
        }
        public float Inertia
        {
            get => inertia;
            set => inertia = value;
        }
    }
}
