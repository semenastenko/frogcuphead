using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Zenject;
using Kudos.GameControllers;

public class TestUIController : MonoBehaviour
{
    [Inject] IGameManager manager;
    public GameObject startPanel;
    public GameObject restartPanel;
    public GameObject completePanel;
    public Button startButton;
    public Button[] restartButton;
    public Button nextButton;

    private void Start()
    {
        manager.Rebuild();
        startPanel.SetActive(true);
        startButton.onClick.AddListener(() =>
        {
            manager.StartGame();
            startPanel.SetActive(false);
        });
        manager.OnEndGame += ()=>
        {
            restartPanel.SetActive(true);
        };
        foreach (var item in restartButton)
        {
            item.onClick.AddListener(() =>
            {
                manager.Rebuild();
                restartPanel.SetActive(false);
                completePanel.SetActive(false);
                startPanel.SetActive(true);
            });
        }
        manager.OnCompleteGame += CompleteLevel;
        nextButton.onClick.AddListener(() =>
        {
            int index = SceneManager.GetActiveScene().buildIndex + 1;
            if (SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings - 1)
            {
                index = 0;
            }
            SceneManager.LoadScene(index);
        });
    }
    public void CompleteLevel()
    {
        completePanel.SetActive(true);
    }
}
