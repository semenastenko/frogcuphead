using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Kudos.GameObjects;

public class GameControl : MonoBehaviour
{
    [Inject] IActivatable[] activatables;
    public Button activateButton;
    public Button dashButton;

    private void Start()
    {
        activateButton.onClick.AddListener(()=> 
        {
            foreach (var item in activatables)
            {
                item.Activate();
            }
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            activateButton.onClick.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            dashButton.onClick.Invoke();
        }
    }
}
