using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Kudos.SO;

public class TestSettings : MonoBehaviour
{
    public Slider slider;
    public Text valueText;
    [Inject] FlightMovementSettings Settings;

    private void Start()
    {
        slider.onValueChanged.AddListener(OnSliderValueChangerd);
        float roundValue = PlayerPrefs.GetFloat("sensetivity", 1);
        valueText.text = roundValue.ToString();
        Settings.Sensitivity = roundValue;
    }

    private void OnSliderValueChangerd(float value)
    {
        float roundValue = (int)(value * 10) / 10f;
        valueText.text = roundValue.ToString();
        Settings.Sensitivity = roundValue;
        PlayerPrefs.SetFloat("sensetivity", roundValue);
    }
}
