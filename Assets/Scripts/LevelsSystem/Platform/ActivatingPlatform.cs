using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{ 
    public class ActivatingPlatform : MonoBehaviour, IDisposable, IManageable, IActivatable
    {
        //IDisposable dispose;

        [SerializeField]
        private bool startActivate;

        private bool isActive;

        public void Rebuild()
        {
            isActive = startActivate;
            gameObject.SetActive(isActive);
        }
        public void StartExecution()
        {

        }

        public void Activate()
        {
            isActive = !isActive;
            gameObject.SetActive(isActive);
        }
        public void FinishExecution()
        {
            //dispose?.Dispose();
        }

        public void Dispose()
        {
            FinishExecution();
        }
    }
}
