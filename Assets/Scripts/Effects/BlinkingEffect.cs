using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.ShadersAPI
{
    public class BlinkingEffect : MonoBehaviour
    {
        [SerializeField]
        private Material _material;
        public bool isBlink { get; private set; }
        public void Blink(bool value)
        {
            isBlink = value;
            if (value)
            {
                _material.color = Color.clear;
            }
            else
            {
                _material.color = Color.white;
            }
        }
    }
}
