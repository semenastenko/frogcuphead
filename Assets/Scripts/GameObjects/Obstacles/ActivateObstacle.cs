using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Kudos.GameObjects
{
    public class ActivateObstacle : Obstacle, ILockable
    {
        [SerializeField]
        private Transform targetA;
        [SerializeField]
        private Transform targetStart;

        public float speed = 1;

        public Vector3 PositionA { get; private set; }
        public Vector3 PositionStart { get; private set; }
        
        private Vector3 target;
        
        private IDisposable moveDispose;


        private void Start()
        {
            PositionA = targetA.position;
            PositionStart = transform.position;
            targetStart.position = PositionStart;
        }

        public void Init(bool value)
        {
            moveDispose?.Dispose();
            if (!value)
            transform.position = PositionStart;
            else
                transform.position = PositionA;
        }

        public void Lock(bool value)
        {
            Debug.Log("Activate " + value);
            if (value)
                target = PositionA;
            else
                target = PositionStart;
            moveDispose?.Dispose();
            moveDispose = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
                    targetA.position = PositionA;
                    targetStart.position = PositionStart;
                    if ((target - transform.position).magnitude < 0.01f)
                    {
                        moveDispose?.Dispose();
                    }
                });
        }
    }
}
