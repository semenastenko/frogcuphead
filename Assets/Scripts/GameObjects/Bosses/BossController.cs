using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using Kudos.ShadersAPI;

namespace Kudos.GameObjects.Bosses
{
    public abstract class BossController : Enemy, IInitializable
    {
        protected Collider[] colliders;

        public void Initialize()
        {
            InitializeSelf();
            //_animator = GetComponentInChildren<Animator>();
            //_damageEffect = _animator.GetComponent<BossDamageEffect>();
            colliders = GetComponents<Collider>();
        }

        public abstract void InitializeSelf();

        protected override void RebuildSelf()
        {

        }

        protected override void StartExecutionSelf()
        {
            _animator.enabled = true;
            _animator.Play("Start");
            _animator.SetTrigger("NextPhase");
        }
        protected override void FinishExecutionSelf()
        {
            _animator.enabled = false;
        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {
            _animator.SetTrigger("Death");
        }

        public void SetActiveCollides(bool value)
        {
            foreach (var item in colliders)
            {
                item.enabled = value;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Player player = other.GetComponent<Player>();
            if (player != null && !player.IsInvincible && player.IsAlive)
            {
                player.TakeDamage(name);
            }
        }
    }
}
