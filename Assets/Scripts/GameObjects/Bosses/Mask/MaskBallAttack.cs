using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class MaskBallAttack : Obstacle, IDisposable, IInitializable
    {
        [Inject] MaskBoss _boss;
        IDisposable moveDispose;
        IDisposable timeDispose;
        LTDescr scaleDispose;

        [SerializeField]
        Transform pivot;
        [SerializeField]
        float verticalSpeed = 10;
        [SerializeField]
        float horizontalSpeed = 10;
        [SerializeField]
        float amplitude = 10;
        [SerializeField]
        float minLifeTime = 1;
        [SerializeField]
        float maxLifeTime = 5;
        [SerializeField]
        SphereGun sphereGun;

        public int phase = 1;

        float lifeTime = 5;
        bool willExplode = false;
        int timeOffset = 0;
        public void Initialize()
        {
            gameObject.SetActive(false);
        }

        public void ReInit(Action callback)
        {
            timeOffset = UnityEngine.Random.Range(0, 10);
            gameObject.SetActive(true);
            transform.position = pivot.position;
            gameObject.transform.localScale = new Vector3(0,0, 0.8f);
            willExplode = UnityEngine.Random.Range(0, 2) == 0 ? false : true;
            if (willExplode)
                lifeTime = UnityEngine.Random.Range(minLifeTime, maxLifeTime);
            else
                lifeTime = maxLifeTime*2;
            scaleDispose = LeanTween.scale(gameObject, Vector3.one * 0.8f, 0.5f)
                .setOnComplete(()=> 
                {
                    StartMoving(callback);
                });
        }

        public void Dispose()
        {
            if (this != null)
            {
                gameObject?.SetActive(false);
                transform.position = pivot.position;
            }
            scaleDispose?.pause();
            timeDispose?.Dispose();
            moveDispose?.Dispose();
        }

        private void StartMoving(Action callback)
        {
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ => Move());

            timeDispose?.Dispose();
            timeDispose = Observable.Timer(TimeSpan.FromSeconds(lifeTime))
                .Subscribe(_ =>
                {
                    if (willExplode)
                        sphereGun.StartShoot(_boss);
                    callback?.Invoke();
                    Dispose();
                });
        }

        private void Move()
        {
            Vector3 _newPosition = transform.position;
            _newPosition.x += -horizontalSpeed * Time.deltaTime;
            _newPosition.y += Mathf.Sin((Time.time + timeOffset) * verticalSpeed) * Time.deltaTime * amplitude;
            _newPosition.z = 0;
            transform.position = _newPosition;
        }

        private void OnTriggerEnter(Collider other)
        {
            Player player = other.GetComponent<Player>();
            if (player != null && !player.IsInvincible && player.IsAlive)
            {
                player.TakeDamage(name);
            }
        }
    }
}
