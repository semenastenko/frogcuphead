using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using System;

namespace Kudos.GameObjects.Bosses
{
    public class MaskBoss : BossController
    {
        [Inject] Camera _camera;

        //IDisposable blink
        public override void InitializeSelf()
        {

        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {
            base.PrepareToDie();
        }

        private void LateUpdate()
        {
            transform.LookAt(_camera.transform);
        }
    }
}
