using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.GameObjects.Bosses
{
    public class SpearSpawner : MonoBehaviour, System.IDisposable
    {
        [SerializeField]
        Transform[] rotateSpawnPoints;
        [SerializeField]
        float height;
        [SerializeField]
        float rotationOffset;
        [SerializeField]
        float offset;
        [SerializeField]
        float maxRotObserveTime = 1;
        [SerializeField]
        float minRotObserveTime = 0.2f;
        [SerializeField]
        float maxHorObserveTime = 1;
        [SerializeField]
        float minHorObserveTime = 0.2f;

        [Inject] Spear.Pool _pool;

        List<Spear> spawnedSpear = new List<Spear>();

        public void SpawnRotate()
        {
            Vector3 spawnPoint = rotateSpawnPoints[Random.Range(0, rotateSpawnPoints.Length)].position;
            Spear spear = _pool.Spawn(spawnPoint, Quaternion.identity);
            spear.OnEndLifeTime += Despawn;
            spear.RotateObserveStart(spawnPoint, Vector3.right * rotationOffset, minRotObserveTime, maxRotObserveTime);
            spawnedSpear.Add(spear);
        }

        public void SpawnHorizontal()
        {
            int rand = Random.Range(0, 2);
            Vector3 spawnPoint = Vector3.zero;
            Vector3 offsetDir = Vector3.zero;
            float rotation = 0;
            if (rand == 0)
            {
                spawnPoint = new Vector3(rotateSpawnPoints[0].position.x, height, 0);
                offsetDir = Vector3.up * offset;
                rotation = 90;
            }
            else
            {
                spawnPoint = new Vector3(rotateSpawnPoints[0].position.x, -height, 0);
                offsetDir = Vector3.down * offset;
                rotation = -90;
            }
            Spear spear = _pool.Spawn(spawnPoint + offsetDir, Quaternion.Euler(0,0, rotation));
            spear.OnEndLifeTime += Despawn;
            spear.HorizontalObserveStart(spawnPoint, offsetDir, minHorObserveTime, maxHorObserveTime);
            spawnedSpear.Add(spear);
        }

        private void Despawn(Spear spear)
        {
            _pool.Despawn(spear);
            spawnedSpear.Remove(spear);
        }

        public void Dispose()
        {
            foreach (var spear in spawnedSpear)
            {
                _pool.Despawn(spear);
            }
            spawnedSpear.Clear();
        }
    }
}
