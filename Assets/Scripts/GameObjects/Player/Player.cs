using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using System;
using Kudos.GameLogic;
using Kudos.ShadersAPI;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{
    public abstract class Player : MonoBehaviour, IDisposable, ITargetable, IInitializable, IManageable
    {
        protected Animator _animator;
        protected BlinkingEffect _blinkingEffect;
        protected Collider _collider;
        protected CollisionsController _cc;
        [SerializeField]
        protected Gun gun;

        [SerializeField]
        protected int startHP = 3;
        [SerializeField]
        protected float invincibleFramesDuration = 1f;
        [SerializeField]
        protected float blinkingSpeed = .1f;

        private Vector3 startPos;

        public int StartHP => startHP;
        public bool IsAlive { get; protected set; }
        public bool IsInvincible { get; protected set; }
        public int CurrentHP { get; protected set; }

        public event Action OnInit;
        public event Action OnDied;
        public event Action OnTakeDamage;

        private IDisposable invincibleFramesDispose;

        public abstract void InitializeSelf();
       protected abstract void RebuildSelf();
       protected abstract void StartExecutionSelf();
       protected abstract void FinishExecutionSelf();
        protected abstract void DetectCollision(CollisionInfo collision);

        public void Initialize()
        {
            _collider = GetComponent<Collider>();
            _cc = new CollisionsController(_collider, false);
            _animator = GetComponentInChildren<Animator>();
            _blinkingEffect = _animator.GetComponent<BlinkingEffect>();
            startPos = transform.position;
            InitializeSelf();
        }

        public void Rebuild()
        {
            transform.position = startPos;
            IsAlive = true;
            IsInvincible = false;
            CurrentHP = startHP;
            OnInit?.Invoke();
            RebuildSelf();
        }
        public void StartExecution()
        {
            _cc.OnCollision += DetectCollision;
            gun.StartShoot(this);

            StartExecutionSelf();
        }
        public void FinishExecution()
        {
            _cc.OnCollision -= DetectCollision;
            FinishExecutionSelf();
        }
        public void Dispose()
        {
            DisposeInvincibleFrame();
            FinishExecution();
        }

        public virtual void Hit(Projectile projectile)
        {
            if (projectile.Owner is Player) return;
            TakeDamage(projectile.Owner.name);
        }

        public virtual void TakeDamage(string name, int damage = 1)
        {
            if (!IsAlive) return;
            if (IsInvincible) return;
            Debug.Log("Damaged " + name);
            CurrentHP -= damage;
            OnTakeDamage?.Invoke();
            if (CurrentHP <= 0)
            {
                Die(name);
            }
            else
            {
                ActivateInvincibleFrame();
            }
        }

        protected virtual void Die(string death)
        {
            if (!IsAlive) return;
            Dispose();
            Debug.Log("die " + death);

            IsAlive = false;
            OnDied?.Invoke();
        }
        public bool IsIgnored()
        {
            return IsInvincible;
        }

        private void ActivateInvincibleFrame()
        {
            IsInvincible = true;
            _cc.IsIgnore = true;
            invincibleFramesDispose?.Dispose();
            invincibleFramesDispose = Observable.Interval(TimeSpan.FromSeconds(blinkingSpeed))
                .Finally(() =>
                {
                    IsInvincible = false;
                    _cc.IsIgnore = false;
                    _blinkingEffect.Blink(false);
                })
                .Subscribe(x =>
                {
                    _blinkingEffect.Blink(!_blinkingEffect.isBlink);

                    if (x * blinkingSpeed >= invincibleFramesDuration)
                        invincibleFramesDispose?.Dispose();
                });
        }

        private void DisposeInvincibleFrame()
        {
            _blinkingEffect.Blink(false);
            IsInvincible = false;
            _cc.IsIgnore = false;
        }
    }
}
