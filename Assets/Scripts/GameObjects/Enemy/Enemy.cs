using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.ShadersAPI;
using System;
using UniRx;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{
    public interface IEnemy { }
    public abstract class Enemy : Obstacle, ITargetable, IManageable, IEnemy
    {
        protected Animator _animator;
        protected DamageEffect _damageEffect;
        [SerializeField]
        protected float startHP;

        public float StartHP => startHP;
        public float CurrentHP { get; protected set; }

        public DamageEffect BossDamageEffect => _damageEffect;

        IDisposable effectDispose;
        IDisposable waitDamageDispose;

        public event Action<Enemy> OnDeath;

        //protected void OnDeathnvoke() => OnDeath?.Invoke();

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
            _damageEffect = GetComponentInChildren<DamageEffect>();
        }

       protected abstract void RebuildSelf();
       protected abstract void StartExecutionSelf();
       protected abstract void FinishExecutionSelf();

        public void Rebuild()
        {
            CurrentHP = startHP;
            gameObject.SetActive(true);
            _damageEffect.Effect(false);
            RebuildSelf();
        }
        public void StartExecution()
        {

            StartExecutionSelf();
        }
        public void FinishExecution()
        {
            effectDispose?.Dispose();
            waitDamageDispose?.Dispose();
            FinishExecutionSelf();
        }
        public void Hit(Projectile projectile)
        {
            //Debug.Log("hit " + projectile.Damage);
            if (projectile.Owner is Enemy) return;
            CurrentHP -= projectile.Damage;
            if (CurrentHP <= 0)
                Die();
            else
                PrepareToHit();
        }

        public bool IsIgnored()
        {
            return false;
        }

        protected abstract void PrepareToDie();
        protected virtual void PrepareToHit()
        {
            DamageEffect();
        }

        public void Die()
        {
            PrepareToDie();
            FinishExecution();
            gameObject.SetActive(false);
            AfterDie();
            OnDeath?.Invoke(this);
            MessageBroker.Default.Publish(GoalMessage.Create(GoalMessage.GoalType.enemies));
        }

        protected virtual void AfterDie() { }

        private void DamageEffect()
        {
            if (effectDispose == null)
            {
                effectDispose = Observable.Interval(TimeSpan.FromSeconds(0.1f))
                 .Finally(() =>
                 {
                     _damageEffect.Effect(false);
                     effectDispose = null;
                 })
                 .Subscribe(_ =>
                 {
                     _damageEffect.Effect(!_damageEffect.isEffect);
                 });
            }

            waitDamageDispose?.Dispose();
            waitDamageDispose = Observable.Timer(TimeSpan.FromSeconds(0.15f))
            .Subscribe(_ =>
            {
                effectDispose?.Dispose();
            });
        }
    }
}
