using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameObjects
{
    public class CommonEnemy : Enemy
    {
        protected override void RebuildSelf()
        {

        }

        protected override void StartExecutionSelf()
        {

        }
        protected override void FinishExecutionSelf()
        {

        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {
            
        }
    }
}
