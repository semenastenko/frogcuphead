using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.GameObjects
{
    public class CameraMovement : MonoBehaviour
    {
        [Inject] private RunPlayer player;

        [SerializeField, Range(0f, 100f)]
        float smoothTime = 0.5f;
        [SerializeField]
        float offset = 0;
        Vector3 velocity = Vector3.zero;

        private void FixedUpdate()
        {
            Move();
        }

        private void Move()
        {
            Vector3 desiredPosition = new Vector3(player.transform.position.x + offset, transform.position.y, transform.position.z);
            Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothTime * Time.fixedDeltaTime);
            transform.position = smoothedPosition;
            // transform.position = desiredPosition;
        }
    }
}
