using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using UniRx;

namespace Kudos.GameObjects
{
    public class TakebleCoins : TakeableObject
    {
        public override void Init()
        {
            gameObject.SetActive(true);
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            gameObject.SetActive(false);
            MessageBroker.Default.Publish(GoalMessage.Create(GoalMessage.GoalType.coins));
        }
    }
}
