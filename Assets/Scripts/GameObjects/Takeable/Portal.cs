using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using UniRx;

namespace Kudos.GameObjects
{
    public class Portal : TakeableObject
    {
        public Portal connectedPortal;

        private bool isTeleporting = false;
        public override void Init()
        {
            isTeleporting = false;
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            Teleport(picker as RunPlayer, collision);
        }

        private void Teleport(RunPlayer player, CollisionInfo collision)
        {
            if (player != null && !isTeleporting)
            {
                isTeleporting = true;
                Vector3 newPos = player.transform.position;
                Vector3 offset = Vector3.zero;
                if (collision.collisionSide == CollisionSideType.left || collision.collisionSide == CollisionSideType.right)
                {
                    offset = new Vector3(-0.5f, transform.position.y - player.transform.position.y,0);
                }
                else if (collision.collisionSide == CollisionSideType.above || collision.collisionSide == CollisionSideType.below)
                {
                    offset = new Vector3(transform.position.x - player.transform.position.x, -0.5f, 0);
                }
                Debug.Log("offset " + offset + " " + collision.collider.name);
                newPos = connectedPortal.transform.position - offset;
                player.transform.position = newPos;
                Observable.Timer(System.TimeSpan.FromSeconds(0.1f))
                    .Subscribe(_=> 
                    {
                        isTeleporting = false;
                    });
            }
        }
    }
}
