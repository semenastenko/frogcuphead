using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;

namespace Kudos.GameObjects
{
    public class Spring : TakeableObject
    {
        public float timeToJumpApex = 0.4f;
        public override void Init()
        {
            _collider.enabled = true;
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            _collider.enabled = false;
            ThrowUp(picker as RunPlayer);
        }

        private void ThrowUp(RunPlayer player)
        {
            if (player != null)
            {
                player.Jump(timeToJumpApex);
            }
        }
    }
}
