using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

namespace Kudos.GameObjects
{
    public abstract class Gun : MonoBehaviour, IDisposable
    {
        [Inject]
        private GameControllers.PoolsManager _poolsManager;
        protected Projectile.Pool targetPool;
        [SerializeField]
        protected float deltaShootTime = 0.2f;
        [SerializeField]
        protected Transform gunpoint;
        [SerializeField, Header("Projectile")]
        protected int poolSize = 10;
        [SerializeField]
        protected Projectile bulletPrefab;

        protected UnityEngine.Object owner;

        protected List<Projectile> activeProjectiles;

        public float DeltaShootTime => deltaShootTime;
        public int PoolSize => poolSize;
        public Projectile BulletPrefab => bulletPrefab;

        [Inject]
        public void Construct()
        {
            if (activeProjectiles == null)
                activeProjectiles = new List<Projectile>();
            if (activeProjectiles.Count != 0)
            {
                Dispose();
            }

            targetPool = _poolsManager.InstantiatePool(BulletPrefab, PoolSize, "Container - " + BulletPrefab.name + "(" + this.name + ")");

            InitSelf();
        }

        protected abstract void InitSelf();
        public abstract void StartShoot(UnityEngine.Object owner);
        public abstract void StopShoot();
        protected abstract void Shot();
        protected abstract void DisposeSelf();

        public void Dispose()
        {
            DisposeSelf();
            if (activeProjectiles != null)
            {
                foreach (var projectile in activeProjectiles)
                {
                    targetPool.Despawn(projectile);
                }
                activeProjectiles.Clear();
            }
        }
    }
}
