using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameObjects;


namespace Kudos.GameObjects
{
    public class PlaneBullet : Projectile
    {
        IDisposable moveDispose;
        protected override void Shooting()
        {
            moveDispose = Observable.EveryFixedUpdate()
                .Subscribe(_ => Move());
        }

        protected override void Movement()
        {
            velocity.x = MoveSpeed * Time.deltaTime;
            velocity.y = 0;
            velocity.z = 0;
            //_mc.Move(velocity);
            transform.Translate(velocity);
        }

        protected override void DestroyedSelf()
        {
            moveDispose?.Dispose();
        }
    }
}

