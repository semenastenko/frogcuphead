using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameObjects
{
    public interface ITargetable
    {
        public bool IsIgnored();
        public void Hit(Projectile projectile);
    }
}
