using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameLogic
{
    [System.Serializable]
    public struct ClampPlace
    {
        public float minClampX;
        public float maxClampX;
        public float minClampY;
        public float maxClampY;

        public ClampPlace(float minClampX, float maxClampX, float minClampY, float maxClampY)
        {
            this.minClampX = minClampX;
            this.maxClampX = maxClampX;
            this.minClampY = minClampY;
            this.maxClampY = maxClampY;
        }

        public static ClampPlace UnClamp()
        {
            ClampPlace clamp = new ClampPlace();
            clamp.minClampX = -float.MaxValue;
            clamp.maxClampX = float.MaxValue;
            clamp.minClampY = -float.MaxValue;
            clamp.maxClampY = float.MaxValue;
            return clamp;
        }
    }
    public class DragTranslateMovement
    {   
        private Camera _camera;
        private List<Touch> filteredFingers;
        private Vector3 remainingTranslation;

        SO.FlightMovementSettings FlightSettings;
        public float Sensitivity => FlightSettings.Sensitivity;
        public float Damping => FlightSettings.Damping;
        public float Inertia => FlightSettings.Inertia;

        private ClampPlace clamp;

        public DragTranslateMovement()
        {
            _camera = Camera.main;
            FlightSettings = new SO.FlightMovementSettings();
            clamp = ClampPlace.UnClamp();
        }

        public DragTranslateMovement(SO.FlightMovementSettings flightSettings, ClampPlace clamp)
        {
            _camera = Camera.main;
            FlightSettings = flightSettings;
            this.clamp = clamp;
        }

        public void Move(Transform transform)
        {
            var oldPosition = transform.localPosition;

            var fingers = UpdateAndGetFingers();

            var screenDelta = GetScreenDelta(fingers);

            if (screenDelta != Vector2.zero)
            {
                Translate(screenDelta, transform);
            }

            // Increment
            remainingTranslation += transform.localPosition - oldPosition;

            // Get t value
            var factor = GetDampenFactor(Damping, Time.deltaTime);

            // Dampen remainingDelta
            var newRemainingTranslation = Vector3.Lerp(remainingTranslation, Vector3.zero, factor);

            // Shift this transform by the change in delta
            transform.localPosition = oldPosition + remainingTranslation - newRemainingTranslation;

            Vector3 pos = transform.position;
            transform.position = new Vector3(Mathf.Clamp(pos.x, clamp.minClampX, clamp.maxClampX),
                Mathf.Clamp(pos.y, clamp.minClampY, clamp.maxClampY), pos.z);

            if (fingers.Count == 0 && Inertia > 0.0f && Damping > 0.0f)
            {
                newRemainingTranslation = Vector3.Lerp(newRemainingTranslation, remainingTranslation, Inertia);
            }

            // Update remainingDelta with the dampened value
            remainingTranslation = newRemainingTranslation;
        }

        private void Translate(Vector2 screenDelta, Transform transform)
        {

            if (_camera != null)
            {
                // Screen position of the transform
                var screenPoint = _camera.WorldToScreenPoint(transform.position);

                // Add the deltaPosition
                screenPoint += (Vector3)screenDelta * Sensitivity;

                // Convert back to world space
                Vector3 pos = _camera.ScreenToWorldPoint(screenPoint);
                transform.position =  new Vector3(Mathf.Clamp(pos.x, clamp.minClampX, clamp.maxClampX), 
                    Mathf.Clamp(pos.y, clamp.minClampY, clamp.maxClampY), pos.z);
            }
        }

        private List<Touch> UpdateAndGetFingers()
        {
            if (filteredFingers == null)
            {
                filteredFingers = new List<Touch>();
            }

            filteredFingers.Clear();

            filteredFingers.AddRange(GetFingers());

            return filteredFingers;
        }

        private List<Touch> GetFingers()
        {
            List<Touch> Fingers = new List<Touch>();

            for (int i = 0; i < Input.touchCount; i++)
            {
                var touch = Input.GetTouch(i);

                Fingers.Add(touch);
            }

#if UNITY_EDITOR
            for (int i = 0; i < SimulatedInput.touchCount; i++)
            {
                var touch = SimulatedInput.GetTouch(i);

                Fingers.Add(touch);
            }
#endif

            return Fingers;
        }

        private Vector2 GetScreenDelta(List<Touch> fingers)
        {
            var delta = default(Vector2);
            TryGetScreenDelta(fingers, ref delta);
            return delta;
        }

        private bool TryGetScreenDelta(List<Touch> fingers, ref Vector2 delta)
        {
            if (fingers != null)
            {
                var total = Vector2.zero;
                var count = 0;

                for (var i = fingers.Count - 1; i >= 0; i--)
                {
                    var finger = fingers[i];

                    //if (finger != null)
                    //{
                    total += finger.deltaPosition;
                    count += 1;
                    //}
                }

                if (count > 0)
                {
                    delta = total / count; return true;
                }
            }

            return false;
        }

        private float GetDampenFactor(float damping, float elapsed)
        {
            if (damping < 0.0f)
            {
                return 1.0f;
            }

#if UNITY_EDITOR
            if (Application.isPlaying == false)
            {
                return 1.0f;
            }
#endif

            return 1.0f - Mathf.Exp(-damping * elapsed);
        }
    }
}
