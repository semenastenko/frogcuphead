using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.SO
{
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "Installers/SOInstaller")]
    public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
    {
        public WeaponsSettingsSO _weaponsSettings;
        public FlightMovementSettings _flightSettings;

        public override void InstallBindings()
        {
            Container.BindInstances(_weaponsSettings);
            Container.BindInstances(_flightSettings);
        }
    }
}
